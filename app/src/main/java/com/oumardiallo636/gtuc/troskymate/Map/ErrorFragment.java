package com.oumardiallo636.gtuc.troskymate.Map;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oumardiallo636.gtuc.troskymate.R;
import com.oumardiallo636.gtuc.troskymate.Utility.MyFragmentList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ErrorFragment extends Fragment {

    private View view;

    public ErrorFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.tv_error_message)
    TextView errorMessage;

    @OnClick(R.id.btn_err_again)
    void try_again(View view){
        MainActivity.getInstance().switchHeaderFragment(MyFragmentList.CUSTOM_TOOLBAR,null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_error, container, false);
        ButterKnife.bind(this,view);

        errorMessage.setText(getArguments().getString(MyFragmentList.ERROR_MESSAGE));

        return view;
    }

}
