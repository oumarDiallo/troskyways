package com.oumardiallo636.gtuc.troskymate.Utility;

/**
 * Created by oumar on 3/29/18.
 */

public class MyFragmentList {

    public static final String BUS_STOP_FRAG = "StartFragment";
    public static final String ORIGIN_STOP = "ShowDirection";
    public static final String START_DIRECTION = "StartDirection";
    public static final String CUSTOM_TOOLBAR = "CustomToolBar";
    public static final String NEXT_STOP_INFO = "NextStopInfo";
    public static final String BOTTOM_DISTANCE_TIME_FRAG = "BottomDistanceTimeFragment";
    public static final String LOADING_FRAGMENT = "LoadingFragment";
    public static final String ERROR_FRAGMENT = "ErrorFragment";

    public static final String ERROR_MESSAGE = "error_message";
}
