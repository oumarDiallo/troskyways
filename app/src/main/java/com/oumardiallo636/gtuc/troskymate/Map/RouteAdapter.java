package com.oumardiallo636.gtuc.troskymate.Map;

import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oumardiallo636.gtuc.troskymate.R;

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.MyViewHolder>{

    private String[] mDataSet;
    private BottomSheetBehavior mBottomsheet;

    private static final String TAG = "RouteAdapter";

    public RouteAdapter(String[] mDataSet,
                        BottomSheetBehavior bottomSheetBehavior) {
        this.mDataSet = mDataSet;
        this.mBottomsheet = bottomSheetBehavior;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: starts");


        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.alternate_route_layout,parent,false);


//        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(view);

        Log.d(TAG, "onCreateViewHolder: ends");
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;


        public MyViewHolder(View v) {
            super(v);
//            mTextView = v;
        }
    }
}
