package com.oumardiallo636.gtuc.troskymate.Map;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

public class CustomRvLayoutManager extends LinearLayoutManager {

    private boolean isScrollable = true;

    public CustomRvLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public void setScrollable(boolean scrollable) {

        isScrollable = scrollable;
    }

    @Override
    public boolean canScrollHorizontally() {

        return isScrollable && super.canScrollHorizontally();
    }
}
