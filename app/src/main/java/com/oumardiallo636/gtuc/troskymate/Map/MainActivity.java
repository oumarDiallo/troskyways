package com.oumardiallo636.gtuc.troskymate.Map;

/**
 * Created by oumar on 9/10/17.
 */

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.transition.Fade;
import android.support.transition.Scene;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.PolyUtil;
import com.oumardiallo636.gtuc.troskymate.Animation.MapAnimator;
import com.oumardiallo636.gtuc.troskymate.Entities.CloseBusStop.BusStop;
import com.oumardiallo636.gtuc.troskymate.Entities.CloseBusStop.CloseStops;
import com.oumardiallo636.gtuc.troskymate.Entities.Direction.Stop;
import com.oumardiallo636.gtuc.troskymate.Entities.Direction.WalkingPoints;
import com.oumardiallo636.gtuc.troskymate.R;
import com.oumardiallo636.gtuc.troskymate.Utility.DayTimeSecondConverter;
import com.oumardiallo636.gtuc.troskymate.Utility.MyFragmentList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION;
import static com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION;
import static com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE;

/**
 * Using location settings.
 * <p/>
 * Uses the {@link com.google.android.gms.location.SettingsApi} to ensure that the device's system
 * settings are properly configured for the app's location needs. When making a request to
 * Location services, the device's system settings may be in a state that prevents the app from
 * obtaining the location data that it needs. For example, GPS or Wi-Fi scanning may be switched
 * off. The {@code SettingsApi} makes it possible to determine if a device's system settings are
 * adequate for the location request, and to optionally invoke a dialog that allows the user to
 * enable the necessary settings.
 * <p/>
 * This sample allows the user to request location updates using the ACCESS_FINE_LOCATION setting
 * (as specified in AndroidManifest.xml).
 */

public class MainActivity extends BaseActivity implements
        OnMapReadyCallback,
        MapActivityMVP.View {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static MapActivityMVP.View mView;
    private DrawerLayout mDrawerlayout;
    private BottomSheetBehavior mBottomsheet;

    private GoogleMap mMap;
    private Marker mMaker;
    private Marker mOriginMaker;
    private List<Marker> markerList;
    private boolean mCameraOnLocation = true;


    private List<Polyline> mStraightPolyline = new ArrayList<>();
    private List<Polyline> mDotedPolylines = new ArrayList<>();

    private final int polyColor = Color.parseColor("#25864b");


    private ArrayList<String> closeStopNameList;
    private ArrayList<String> closeStoplocationList;


    private String destinationName;
    private Fragment mCurrentBottomFragment;
    private Fragment mCurrentHeaderFragment;

    private int mMapPadding;

    private GeofencingClient mGeofencingClient;
    private ArrayList<Geofence> mGeofenceList;
    private PendingIntent mGeofencePendingIntent;
    private ArrayList<Stop> mListBusStops;

    private Stop mCurrentStop;
    private Stop mNextStop;
    private Stop mThirdStop;

    private boolean isNavigationOn;
    private boolean isCurrentLocationSeen;
    private int mReason;

    private Stop mDestinationStop;


    Vibrator vibrator;
    ProgressDialog mDialog;

    /**
     * Get reference to the presenter which contain the business logic of the app
     **/
    Presenter presenter;


    private TextView search;


    /**
     * Navigation floating action buttons
     */
    @BindView(R.id.floatingActionButton)
    FloatingActionButton navigation;

    @BindView(R.id.fab_left_one)
    FloatingActionButton btnCenterRoute;

    @BindView(R.id.fab_left_two)
    FloatingActionButton btnShowSteps;


    @OnClick(R.id.floatingActionButton)
    public void click() {

        navigation.setVisibility(View.INVISIBLE);

        LatLng target = new LatLng(presenter.getCurrentLocation().getLatitude(),
                presenter.getCurrentLocation().getLongitude());

        moveCameraToLocation(target, 2);
    }

    @OnClick(R.id.fab_left_one)
    public void showPath() {
        centerRoute();
    }

    @OnClick(R.id.fab_left_two)
    public void showSteps() {
        mBottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    /**
     * End navigation base floating action buttons
     */

    public static MapActivityMVP.View getInstance() {
        return mView;
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.main_activity);

        vibrator = (Vibrator) this.getSystemService(this.VIBRATOR_SERVICE);

        mDrawerlayout = findViewById(R.id.drawer);

        ButterKnife.bind(this);


        mView = this;

//        activateToolbar(false);

        Log.d(TAG, "onCreate: starts");

        presenter = new Presenter();

        // Update values using data stored in the Bundle.
        presenter.updateValuesFromBundle(savedInstanceState);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Log.d(TAG, "onCreate: ends");


        CustomToolBar customToolBar = new CustomToolBar();
        mCurrentHeaderFragment = customToolBar;

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container_header_layout, customToolBar)
                .commitAllowingStateLoss();

        mDialog = new ProgressDialog(this);
        mGeofencingClient = LocationServices.getGeofencingClient(this);


        //Side navigation based logic
        NavigationView navigationView = findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        // set item as selected to persist highlight
                        item.setChecked(true);

                        //this closes the drawers
                        mDrawerlayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });

    }

    /**
     * The following Method uses the GeofencingRequest class and its nested
     * GeofencingRequestBuilder class to specify the geofences to monitor and to
     * set how related geofence events are triggered:
     *
     * @return
     */

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: starts");
        // Within {@code onPause()}, we remove location updates. Here, we resume receiving
        // location updates if the user has requested them.

        presenter.createLocationCallback();
        presenter.createLocationRequest();
        presenter.buildLocationSettingsRequest();

        if (presenter.getRequestingLocationUpdates() && presenter.checkPermissions()) {
            presenter.startLocationUpdates();
        } else if (!presenter.checkPermissions()) {
            presenter.requestPermissions();
        }
        presenter.startLocationUpdates();
        Log.d(TAG, "onResume: ends");
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Remove location updates to save battery.
        presenter.stopLocationUpdates();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: starts");
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
//                        mRequestingLocationUpdates = false;
////                        updateUI();
//                        startLocationUpdates();
                        break;
                }
                break;
            case PLACE_AUTOCOMPLETE_REQUEST_CODE:

                if (resultCode == RESULT_OK) {

                    clearMap();

                    Place place = PlaceAutocomplete.getPlace(this, data);
                    destinationName = (String) place.getName();
                    Double destinationLat = place.getLatLng().latitude;
                    Double destinationLng = place.getLatLng().longitude;

                    // keeping a reference of the final destination

                    StringBuilder destinationBuilder = new StringBuilder();
                    destinationBuilder.append(destinationLat.toString())
                            .append(",")
                            .append(destinationLng.toString());

                    mDestinationStop = new Stop();
                    mDestinationStop.setStopName(destinationName);
                    mDestinationStop.setBusName("END");
                    mDestinationStop.setStopLocation(destinationBuilder.toString());


                    presenter.saveDestination(destinationLat, destinationLng);
                    requestClosestStops();

                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);

                    // TODO: Handle the error.
                    Log.i(TAG, status.getStatusMessage());

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }
        }
    }

    public void searchDirection(String origin) {

        String destination = presenter.getStringDestination();

        presenter.getDirection(origin, destination.toString());
    }

    /**
     * This method is called when user clicks on toolbar's search field .
     */
    @Override
    public void searchPlace(View view) {
        Log.d(TAG, "searchPlace: tts");

        presenter.findDestination();
    }

    public void trigger(View view) {
        mDrawerlayout.openDrawer(GravityCompat.START);
    }


    /**
     * Sets the value of the UI fields for the location latitude, longitude and last update time.
     */
    @Override
    public void updateLocationUI(Location currentLocation) {
        Log.d(TAG, "updateLocationUI: starts");
        Log.d(TAG, "updateLocationUI: " + currentLocation);
        if (currentLocation != null) {

            LatLng currentPosition = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            showCurrentLocation();
            if (mCameraOnLocation) {
                moveCameraToLocation(currentPosition, 0);
                mCameraOnLocation = false;
            }
        } else {

        }
        Log.d(TAG, "updateLocationUI: ends");
    }

    /**
     * Stores activity data in the Bundle.
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, presenter.getRequestingLocationUpdates());
        savedInstanceState.putParcelable(KEY_LOCATION, presenter.getCurrentLocation());
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, presenter.getLastUpdateTime());
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    public void showSnackbar(final int mainTextStringId, final int actionStringId,
                             View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }


    @Override
    public void drawPolyline(List<String> route) {

        PolylineOptions options = new PolylineOptions();

        for (String p : route) {
            options.addAll(PolyUtil.decode(p));
        }
//            route.addAll(PolyUtil.decode(p));
        Polyline polyline = mMap.addPolyline(options);
        polyline.setColor(polyColor);
        polyline.setWidth(3);
        mStraightPolyline.add(polyline);
    }

    /**
     * ********************************** GEOFENCE MANAGEMENT  ***********************************
     **/

    @Override
    public void saveRoutesInfo(List<Stop> stops) {
        mListBusStops = new ArrayList<>();
        mListBusStops.addAll(stops);
        Stop currentPosition = new Stop();

        currentPosition.setBusName("walking");
        currentPosition.setStopLocation(presenter.getStringOrigin());

        mCurrentStop = currentPosition;

        mNextStop = mListBusStops.get(0);
    }

    @Override
    public void createGeofences() {
        Log.d(TAG, "createGeofences: starts");


        mGeofenceList = new ArrayList<>();


        double lat0 = Double.parseDouble(mListBusStops.get(0).getStopLocation().split(",")[0]);
        double lng0 = Double.parseDouble(mListBusStops.get(0).getStopLocation().split(",")[1]);

        // The first bus stop is always at a walkable distance so configure it geofence to 20 meters

        mGeofenceList.add(new Geofence.Builder()
                .setRequestId("0")
                .setCircularRegion(
                        lat0,
                        lng0,
                        20
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                .build());

        // starting from the second bus stop we need to check if the previous stop was at a walkable
        //distance to it, then decide whether we need a 20 meter radius or 80 meter radius
        // not that we add 80 meters radius when we need to take a bus from the previous stop to
        // the current stop

        for (int i = 1; i < mListBusStops.size(); i++) {

            double lat = Double.parseDouble(mListBusStops.get(i).getStopLocation().split(",")[0]);
            double lng = Double.parseDouble(mListBusStops.get(i).getStopLocation().split(",")[1]);

            Log.d(TAG, "createGeofences: " + mListBusStops.get(i).getStopName());

            if (mListBusStops.get(i - 1).getBusName().equalsIgnoreCase("walking")) {

                mGeofenceList.add(new Geofence.Builder()
                        .setRequestId(i + "")
                        .setCircularRegion(lat, lng, 40)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .build());

            } else {

                mGeofenceList.add(new Geofence.Builder()
                        .setRequestId(i + "")
                        .setCircularRegion(lat, lng, 80)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .build());
            }

        }

        Log.d(TAG, "createGeofences: ends");
    }


    @Override
    public void geofenceEnterResponse(final String message) {

        Log.d(TAG, "geofenceResponse: starts");

        final int currentStopId = Integer.parseInt(message);

        mCurrentStop = mListBusStops.get(currentStopId);

        if ((currentStopId + 1) < mListBusStops.size()) {
            mNextStop = mListBusStops.get(currentStopId + 1);

        } else {
            mNextStop = null;
        }

        if ((currentStopId + 2) < mListBusStops.size()) {
            mThirdStop = mListBusStops.get(currentStopId + 2);

        } else {
            mThirdStop = null;
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(2000, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    vibrator.vibrate(2000);
                }

                Log.d(TAG, "geofenceEnterResponse: here");


                if (currentStopId != 0) {

                    if (!mListBusStops.get(currentStopId - 1).getBusName().equalsIgnoreCase("walking")) {

                        changeNextStopInfo(0);
                    }
                }

                Handler handler = new Handler();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "geofenceEnterResponse: In");
                        changeNextStopInfo(1);
                    }
                }, 10000);

                Log.d(TAG, "geofenceEnterResponse: here 2");
            }
        });

        removeAGeofence(message);

        Log.d(TAG, "geofenceResponse: ends");
    }


    public void changeNextStopInfo(int type) {

        Log.d(TAG, "changeNextStopInfo: starts");

        if (mCurrentHeaderFragment instanceof NextStopInfo) {

            final TextView instruction = findViewById(R.id.tv_info);
            final ImageView icon = findViewById(R.id.im_direction_icon);
            final TextView stopName = findViewById(R.id.tv_next_stop_name);
            final TextView thenInfo = findViewById(R.id.tv_then);


            if (type == 1) {

                if (mNextStop != null) {

                    double originLat = presenter.getCurrentLocation().getLatitude();
                    double originLng = presenter.getCurrentLocation().getLongitude();

                    double destinationLat = Double.parseDouble(mNextStop.getStopLocation().split(",")[0]);
                    double destinationLng = Double.parseDouble(mNextStop.getStopLocation().split(",")[1]);


                    if (mCurrentStop.getBusName().equalsIgnoreCase("walking")) {

                        Log.d(TAG, "changeNextStopInfo: " + mCurrentStop.getStopName());
                        Log.d(TAG, "changeNextStopInfo: " + mCurrentStop.getBusName());

                        instruction.setText("WALK TO");
                        icon.setImageResource(R.drawable.ic_directions_walk_24dp);

                    } else {

                        icon.setImageResource(R.drawable.ic_directions_bus_24dp);
                        instruction.setText("TAKE " + mCurrentStop.getBusName() + " TO");
                    }

                    stopName.setText(mNextStop.getStopName());

                    if (mNextStop.getBusName().equalsIgnoreCase("walking")) {

                        Log.d(TAG, "changeNextStopInfo: 3 stop walk");
                        Drawable img = getApplicationContext()
                                .getResources()
                                .getDrawable(R.drawable.ic_directions_walk_24dp);

                        img.setBounds(0, 0, 60, 60);
                        thenInfo.setCompoundDrawables(null, null, img, null);

                    } else if (mNextStop.getBusName().equalsIgnoreCase("end")) {

                        thenInfo.setVisibility(View.INVISIBLE);
                    } else {

                        Log.d(TAG, "changeNextStopInfo: 3 stop bus");
                        Drawable img = getApplicationContext()
                                .getResources()
                                .getDrawable(R.drawable.ic_directions_bus_24dp);

                        img.setBounds(0, 0, 60, 60);
                        thenInfo.setCompoundDrawables(null, null, img, null);
                    }

                } else {

                    instruction.setText("CONGRATULATION");
                    stopName.setText("YOU HAVE ARRIVED");
                    icon.setVisibility(View.INVISIBLE);
                    thenInfo.setVisibility(View.INVISIBLE);
                }

            } else {

                instruction.setText("ALIGHT At");
                stopName.setText(mCurrentStop.getStopName());

            }

        }

        Log.d(TAG, "changeNextStopInfo: ends");
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }

        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    @Override
    public void startGeofences() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences added
                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to add geofences
                        // ...

                        Toast.makeText(MainActivity.this, "Failure to add geofence", Toast.LENGTH_LONG);
                    }
                });
    }

    @Override
    public void removeAGeofence(final String message) {

        Log.d(TAG, "removeAGeofence: starts");

        List<String> toRemove = new ArrayList<String>();

//        toRemove.add(message);

        int value = Integer.parseInt(message);

        for (Integer i = value; i >= 0; i--) {
            toRemove.add(i.toString());
        }

        mGeofencingClient.removeGeofences(toRemove)
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences removed
                        // ...
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, message + " removed", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to remove geofences
                        // ...
                    }
                });
    }

    /**
     * ****************************** FRAGMENT MANAGEMENT *************************************
     **/

    public void hideHeaderFragment() {

        if (mCurrentHeaderFragment instanceof CustomToolBar) {
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.custom_toolbar_slide_out, R.anim.custom_toolbar_slide_in)
                    .hide(mCurrentHeaderFragment)
                    .remove(mCurrentHeaderFragment)
                    .commitAllowingStateLoss();
        } else if (mCurrentHeaderFragment instanceof NextStopInfo) {
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.loader_slide_in, R.anim.show_direction_slide_out)
                    .hide(mCurrentHeaderFragment)
                    .remove(mCurrentHeaderFragment)
                    .commitAllowingStateLoss();
        } else if (mCurrentHeaderFragment instanceof LoadingFragment) {
            getSupportFragmentManager().beginTransaction()
                    .hide(mCurrentHeaderFragment)
                    .remove(mCurrentHeaderFragment)
                    .commitAllowingStateLoss();
        } else if (mCurrentHeaderFragment instanceof ErrorFragment) {
            getSupportFragmentManager().beginTransaction()
                    .hide(mCurrentHeaderFragment)
                    .remove(mCurrentHeaderFragment)
                    .commitAllowingStateLoss();
        }
    }

    @Override
    public void switchHeaderFragment(String toFragment, Bundle bundle) {

        Log.d(TAG, "switchRecyclerViewContent: starts");
        //animate hiding of the old fragment

        if (mCurrentHeaderFragment != null) {
            hideHeaderFragment();
        }
        switch (toFragment) {

            case MyFragmentList.LOADING_FRAGMENT:
                Log.d(TAG, "switchRecyclerViewContent: called");

                isNavigationOn = false;

                LoadingFragment loadingFragment = new LoadingFragment();
                mCurrentHeaderFragment = loadingFragment;

                loadingFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container_header_layout, loadingFragment)
                        .commitAllowingStateLoss();

                break;
            case MyFragmentList.CUSTOM_TOOLBAR:
                Log.d(TAG, "switchRecyclerViewContent: called");

                isNavigationOn = false;

                CustomToolBar customToolBar = new CustomToolBar();
                mCurrentHeaderFragment = customToolBar;

                customToolBar.setArguments(bundle);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container_header_layout, customToolBar)
                        .setCustomAnimations(R.anim.show_direction_slide_in, R.anim.show_direction_slide_out)
                        .commitAllowingStateLoss();

                search = findViewById(R.id.tv_search);

                break;

            case MyFragmentList.NEXT_STOP_INFO:

                isNavigationOn = true;

                NextStopInfo nextStopInfo = new NextStopInfo();
                mCurrentHeaderFragment = nextStopInfo;

                Bundle bundle1 = new Bundle();
                bundle1.putString("next_stop_name", mNextStop.getStopName());

                nextStopInfo.setArguments(bundle1);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container_header_layout, nextStopInfo)
                        .commitAllowingStateLoss();

                LatLng target = new LatLng(presenter.getCurrentLocation().getLatitude(),
                        presenter.getCurrentLocation().getLongitude());

                moveCameraToLocation(target, 2);
                break;
            case MyFragmentList.ERROR_FRAGMENT:

                isNavigationOn = false;

                ErrorFragment errorFragment = new ErrorFragment();
                mCurrentHeaderFragment = errorFragment;

                errorFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container_header_layout, errorFragment)
                        .commitAllowingStateLoss();

                break;
            default:
                break;
        }
        Log.d(TAG, "switchHeaderFragment: ends");
    }

    @Override
    public void switchRecyclerViewContent(String toFragment, Bundle bundle) {

        final RelativeLayout recyclerViewContainer = findViewById(R.id.rv_container);

        mBottomsheet = BottomSheetBehavior.from(recyclerViewContainer);
        mBottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);


        RecyclerView mRecyclerView = findViewById(R.id.rv_start_direction);

        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(mRecyclerView);

        final CustomRvLayoutManager mLayoutManager = new CustomRvLayoutManager(this,
                LinearLayoutManager.HORIZONTAL,
                false);

        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        String[] myDataset = {"1", "2", "3"};
        RouteAdapter mAdapter = new RouteAdapter(myDataset, mBottomsheet);
        mRecyclerView.setAdapter(mAdapter);


        if (recyclerViewContainer.getVisibility() != View.VISIBLE) {
            recyclerViewContainer.setVisibility(View.VISIBLE);
        }

        switch (toFragment) {

            case MyFragmentList.START_DIRECTION:
                Log.d(TAG, "start fragment: called");

                btnCenterRoute.setVisibility(View.VISIBLE);
                btnShowSteps.setVisibility(View.VISIBLE);

                mBottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {

                        switch (newState) {
                            case BottomSheetBehavior.STATE_HIDDEN:
                                mCurrentHeaderFragment.getView().setVisibility(View.VISIBLE);
                                clearMap();
                                break;
                            case BottomSheetBehavior.STATE_EXPANDED:
                                mCurrentHeaderFragment.getView().setVisibility(View.INVISIBLE);

                                ViewGroup sceneRoot = (ViewGroup) findViewById(R.id.card_route);
                                Scene  scene = Scene.getSceneForLayout(sceneRoot, R.layout.al_layout_expended, getApplicationContext());

                                Transition transition = new Fade();
                                TransitionManager.go(scene, transition);
                                mLayoutManager.setScrollable(false);

                                break;
                            case BottomSheetBehavior.STATE_COLLAPSED:
                                ViewGroup sceneRoot2 = (ViewGroup) findViewById(R.id.card_route);
                                Scene  scene2 = Scene.getSceneForLayout(sceneRoot2, R.layout.alternate_route_layout, getApplicationContext());

                                Transition transition1 = new Fade();
                                TransitionManager.go(scene2, transition1);
                                mCurrentHeaderFragment.getView().setVisibility(View.INVISIBLE);
                                mLayoutManager.setScrollable(true);

                                break;
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                    }
                });

                break;

            case MyFragmentList.BOTTOM_DISTANCE_TIME_FRAG:
                BottomDistanceTimeFragment fragment = new BottomDistanceTimeFragment();
                mCurrentBottomFragment = fragment;

                Bundle bundle1 = new Bundle();
                bundle1.putString("destination_name", mDestinationStop.getStopName());
                fragment.setArguments(bundle1);

//                getSupportFragmentManager().beginTransaction()
//                        .setCustomAnimations(R.anim.start_slide_in, R.anim.start_slide_out)
//                        .replace(R.id.container_layout, fragment)
//                        .commitAllowingStateLoss();

                break;
            default:
                break;
        }
        Log.d(TAG, "switchRecyclerViewContent: ends");
    }

    /**
     * ********************************** Map Management ************************************
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: starts");
        mMap = googleMap;

//        try {
//
//            // Customise the styling of the base map using a JSON object defined
//            // in a raw resource file.
//            boolean success = googleMap.setMapStyle(
//                    MapStyleOptions.loadRawResourceStyle(
//                            this, R.raw.night_mode));
//
//            if (!success) {
//                Log.e(TAG, "Style parsing failed.");
//            }
//        } catch (Resources.NotFoundException e) {
//            Log.e(TAG, "Can't find style. Error: ", e);
//        }

        showCurrentLocation();
        mMap.setBuildingsEnabled(true);
        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(false);
        uiSettings.setCompassEnabled(false);

        mMap.setPadding(0, 108, 0, 0);


        View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();


        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        rlp.setMargins(0, 40, 30, 0);


        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                Log.d(TAG, "==camera idle==" + mMap.getCameraPosition().target);

                if (presenter.getCurrentLocation() != null) {

                    LatLng currentLocation = new LatLng(presenter.getCurrentLocation().getLatitude(),
                            presenter.getCurrentLocation().getLongitude());

                    isCurrentLocationSeen = mMap.getProjection()
                            .getVisibleRegion()
                            .latLngBounds
                            .contains(currentLocation);

                    Log.d(TAG, "onCameraMoveStarted: " + isCurrentLocationSeen);

                    if (mReason == REASON_GESTURE) {

                        if (isNavigationOn && navigation.getVisibility() == View.INVISIBLE) {

                            if (!isCurrentLocationSeen) {
                                navigation.setVisibility(View.VISIBLE);

                            } else {
                                LatLng target = new LatLng(presenter.getCurrentLocation().getLatitude(),
                                        presenter.getCurrentLocation().getLongitude());
                                moveCameraToLocation(target, 2);
                            }
                        }

                    } else if (mReason == REASON_API_ANIMATION) {

                    } else if (mReason == REASON_DEVELOPER_ANIMATION) {

                    }
                }
            }

        });

        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int reason) {
                mReason = reason;
            }


        });
    }

    @Override
    public void drawMarkers(List<MarkerOptions> markerOptions) {


        markerList = new ArrayList<>();

        Log.d(TAG, "drawMarkers: starts");

        //draw destination marker with different icon
        MarkerOptions destinationMarker = markerOptions.get(markerOptions.size() - 1);
        markerOptions.remove(destinationMarker);

        drawDestinationMarker(destinationName);

        for (MarkerOptions m : markerOptions) {
            Log.d(TAG, "drawMarkers: " + m);
            markerList.add(mMap.addMarker(m.icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.logo_image))));
        }

        Log.d(TAG, "drawMarkers: ends");
    }

    @Override
    public void moveCameraToLocation(LatLng location, int type) {
        Log.d(TAG, "moveCameraToLocation: starts");

        if (type == 0) {

            // this is when we are presenting the destination marker

            CameraPosition cameraPosition = CameraPosition.builder()
                    .target(location)
                    .zoom(16)
                    .bearing(90)
                    .build();
            // Animate the change in camera view over 2 seconds
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),
                    2000, null);
        } else if (type == 1) {

            // this is when we are presenting all the route to the user

            Log.d(TAG, "moveCameraToLocation: called");

            centerRoute();

//            LatLngBounds target = new LatLngBounds(origin, destination);
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(target.getCenter(), 16), 2000, null);

        } else if (type == 2) {

            // This is when we are on navigation mode

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(presenter.getCurrentLocation().getLatitude(),
                            presenter.getCurrentLocation().getLongitude()))      // Sets the center of the map to Mountain View
                    .zoom(18)                   // Sets the zoom
                    .bearing(180)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),
                    2000, null);
        }

    }

    @Override
    public void changeMapBottomPadding(int padding) {
        mMapPadding = padding;
        ValueAnimator animator = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animator = ValueAnimator.ofInt(mMapPadding, padding);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        mMap.setPadding(0, 116, 0, (Integer) valueAnimator.getAnimatedValue());
                    }
                }
            });
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animator.setDuration(5000);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animator.start();
        }
    }

    public void clearMap() {

        mMap.clear();

        if (navigation != null) {
            navigation.setVisibility(View.INVISIBLE);
        }

        LatLng currentPosition = new LatLng(presenter.getCurrentLocation().getLatitude(),
                presenter.getCurrentLocation().getLongitude());

        moveCameraToLocation(currentPosition, 0);

        btnCenterRoute.setVisibility(View.GONE);
        btnShowSteps.setVisibility(View.GONE);

        if (search != null) {
            search.setText(null);
        }

        changeMapBottomPadding(0);
    }


    @Override
    public void showMarker(LatLng location, String type, String name) {

        if (type.equalsIgnoreCase("origin")) {

            drawOriginMarker(location, name);
            moveCameraToLocation(location, 0);

        } else if (type.equalsIgnoreCase("destination")) {

            drawDestinationMarker(name);
            moveCameraToLocation(location, 0);
        }

    }

    private void drawDestinationMarker(String name) {

        LatLng destination = presenter.getDestination();

        if (mMaker != null)
            mMaker.remove(); // remove any existing destination marker from the map

        mMaker = mMap.addMarker(new MarkerOptions() // add a new destination marker to the map
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.destination))
                .title(name)
                .position(destination));

        mMaker.showInfoWindow();

    }


    private void drawOriginMarker(LatLng location, String name) {

        if (mOriginMaker != null)
            mOriginMaker.remove(); // remove any existing destination marker from the map

        mOriginMaker = mMap.addMarker(new MarkerOptions() // add a new destination marker to the map
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.logo_image))
                .title(name)
                .position(location));

        mOriginMaker.showInfoWindow();

    }

    private void startAnim(List<LatLng> route) {
        if (mMap != null) {
            MapAnimator.getInstance().animateRoute(mMap, route);
        } else {
            Toast.makeText(getApplicationContext(), "Map not ready", Toast.LENGTH_LONG).show();
        }
    }

    private void showCurrentLocation() {
        Log.d(TAG, "showCurrentLocation: starts");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if (mCurrentStop != null && mCurrentStop.getBusName().equalsIgnoreCase("walking")) {

            updateNavigationLayout("walking");

        } else {

            updateNavigationLayout("driving");
        }

//        changeNextStopInfo();

        mMap.setMyLocationEnabled(true);

    }

    private void updateNavigationLayout(String mode) {

        Log.d(TAG, "updateNavigationLayout: starts");

        if ((mCurrentHeaderFragment instanceof NextStopInfo)
                && (mCurrentBottomFragment instanceof BottomDistanceTimeFragment)) {

            List<String> origin = new ArrayList<>();

            Location location = presenter.getCurrentLocation();
            Double lat = location.getLatitude();
            Double lng = location.getLongitude();

            StringBuilder originBuilder = new StringBuilder();
            originBuilder.append(lat.toString())
                    .append(",")
                    .append(lng.toString());

            origin.add(originBuilder.toString());

            List<String> destination = new ArrayList<>();

            if (mNextStop != null) {
                destination.add(mNextStop.getStopLocation());
                destination.add(mDestinationStop.getStopLocation());
                presenter.getDistanceAndTime(origin, destination, mode);
            }

            if (navigation.getVisibility() == View.INVISIBLE) {

                LatLng target = new LatLng(presenter.getCurrentLocation().getLatitude(),
                        presenter.getCurrentLocation().getLongitude());
                moveCameraToLocation(target, 2);
            }

        }

    }

    /**
     * ****************************** REQUEST MANAGEMENT **********************************
     */
    private void requestClosestStops() {

        Bundle bundle = new Bundle();
        presenter.getClosestStops();

        switchHeaderFragment(MyFragmentList.LOADING_FRAGMENT, bundle);
    }

    /**
     ********************************* RESPONSE MANAGEMENT **************************************
     * */

    /**
     * Callback received when a list of close bus stops have been returned.
     */
    @Override
    public void responseCloseStops(CloseStops stops) {

        closeStopNameList = new ArrayList<>();
        closeStoplocationList = new ArrayList<>();

        List<BusStop> busStops = stops.getBusStop();
        if (busStops.size() > 0) {

            for (BusStop stop : busStops) {
                closeStopNameList.add(stop.getStopName());
                closeStoplocationList.add(stop.getStopLocation());
            }
        }

        switchHeaderFragment(MyFragmentList.CUSTOM_TOOLBAR, null);

        LatLng destination = presenter.getDestination();

        showMarker(destination, "destination", destinationName);

        ShowDirection showDirection = new ShowDirection();

        Bundle bundle = new Bundle();

        bundle.putStringArrayList("stop_names", closeStopNameList);
        bundle.putStringArrayList("stop_locations", closeStoplocationList);

        showDirection.setArguments(bundle);
        showDirection.show(getSupportFragmentManager(), "showDirection");

    }

    @Override
    public void displayWakingPath(List<WalkingPoints> walkingPoints) {
        Log.d(TAG, "displayWakingPath: starts");

        final List<PatternItem> pattern = Arrays.<PatternItem>asList(
                new Dot(), new Gap(10));

        if (mDotedPolylines.size() != 0) {
            for (Polyline polyline : mDotedPolylines) {
                polyline.remove(); // Remove existing polyline from the map
            }
        }

        for (WalkingPoints path : walkingPoints) {

            String origin = path.getOrigin();
            double originLat = Double.parseDouble(origin.split(",")[0]);
            double originLng = Double.parseDouble(origin.split(",")[1]);
            final LatLng originCoordinates = new LatLng(originLat, originLng);

            String destination = path.getDestination();
            double destiLat = Double.parseDouble(destination.split(",")[0]);
            double destiLng = Double.parseDouble(destination.split(",")[1]);
            final LatLng destinationCoordinates = new LatLng(destiLat, destiLng);

            GoogleDirection.withServerKey(ApplicationRepo.GoogleRepo.apiKey)
                    .from(originCoordinates)
                    .to(destinationCoordinates)
                    .transportMode(TransportMode.WALKING)
                    .execute(new DirectionCallback() {
                        @Override
                        public void onDirectionSuccess(Direction direction, String rawBody) {
                            // Do something here
                            if (direction.isOK()) {

                                Log.d(TAG, "onDirectionSuccess: okk");
                                Route route = direction.getRouteList().get(0);
                                Leg leg = route.getLegList().get(0);
                                ArrayList<LatLng> pointList = leg.getDirectionPoint();

                                PolylineOptions polylineOptions = DirectionConverter.createPolyline(MainActivity.this, pointList, 3, Color.BLUE);
                                polylineOptions.pattern(pattern);
                                polylineOptions.color(polyColor);
                                mMap.addPolyline(polylineOptions);

                            } else {

                                PolylineOptions polylineOptions = new PolylineOptions()
                                        .add(originCoordinates)
                                        .add(destinationCoordinates)
                                        .color(polyColor)
                                        .pattern(pattern);
                                mDotedPolylines.add(mMap.addPolyline(polylineOptions));
                            }
                        }

                        @Override
                        public void onDirectionFailure(Throwable t) {
                            // Do something here
                        }
                    });

        }

        switchHeaderFragment(MyFragmentList.CUSTOM_TOOLBAR, null);

        moveCameraToLocation(null, 1);
        Log.d(TAG, "displayWakingPath: ends");
    }

    @Override
    public void updateNextStopTimeAndDate(long seconds, int distanceMeters) {

        if (mCurrentHeaderFragment instanceof NextStopInfo) {

            String totalDistance;

            if (distanceMeters > 1000) {
                Double distance = distanceMeters / 1000.0;
                totalDistance = distance.toString() + " km";
            } else {
                Integer distance = distanceMeters;
                totalDistance = distance.toString() + " m";
            }
            //convert distance from meters to kilometers

            DayTimeSecondConverter converter = new DayTimeSecondConverter(seconds);
            long second = converter.getSeconds();
            long minute = converter.getMin();
            long hours = converter.getHour();
            int day = converter.getDay();

            TextView time = (TextView) findViewById(R.id.tv_info_time);
            TextView remainingDistance = (TextView) findViewById(R.id.tv_info_distance);

            StringBuilder remainingTime = new StringBuilder();

            if (day != 0) remainingTime.append(day).append("day ");

            if (hours != 0) remainingTime.append(hours).append("h ");

            if (minute != 0) remainingTime.append(minute).append("min ");

            remainingTime.append(second).append(" s");


            if (time != null && remainingDistance != null) {
                time.setText(remainingTime.toString());
                remainingDistance.setText(totalDistance);
            }

        }
    }

    @Override
    public void updateFinalStopTimeAndDate(long seconds, int distanceMeters) {

        Log.d(TAG, "updateFinalStopTimeAndDate: called");

        if (mCurrentBottomFragment instanceof BottomDistanceTimeFragment) {

            String totalDistance;

            if (distanceMeters > 1000) {
                Double distance = distanceMeters / 1000.0;
                totalDistance = distance.toString() + " km";
            } else {
                Integer distance = distanceMeters;
                totalDistance = distance.toString() + " m";
            }

            DayTimeSecondConverter converter = new DayTimeSecondConverter(seconds);
            long second = converter.getSeconds();
            long minute = converter.getMin();
            long hours = converter.getHour();
            int day = converter.getDay();

            TextView time = (TextView) findViewById(R.id.tv_final_time);
            TextView remainingDistance = (TextView) findViewById(R.id.tv_final_distance);

            StringBuilder remainingTime = new StringBuilder();

            if (day != 0) remainingTime.append(day).append("day ");

            if (hours != 0) remainingTime.append(hours).append("h ");

            if (minute != 0) remainingTime.append(minute).append("min ");

            remainingTime.append(second).append("s");


            if (time != null && remainingDistance != null) {
                time.setText(remainingTime.toString());
                remainingDistance.setText(totalDistance);
            }
        }
    }

    @Override
    public void setVisibilityNavigationStartFrag(boolean value) {

//        btnCenterRoute = findViewById(R.id.fab_left_one);
//        btnShowSteps = findViewById(R.id.fab_left_two);

        if (value) {
            btnCenterRoute.setVisibility(View.VISIBLE);
            btnShowSteps.setVisibility(View.VISIBLE);
        } else {
            btnShowSteps.setVisibility(View.GONE);
            btnCenterRoute.setVisibility(View.GONE);
        }
    }


    @Override
    public void loadStartFragment(Integer distance, int seconds) {
        Log.d(TAG, "loadStartFragment: starts");

        switchRecyclerViewContent(MyFragmentList.START_DIRECTION, null);


//        String totalDistance;
//
//        if (distance >= 1000) {
//
//            Double valueInkillo = distance / 1000.0;
//
//            totalDistance = valueInkillo.toString() + " km";
//        } else {
//
//            Integer valueInMeters = distance;
//            totalDistance = valueInMeters.toString() + " m";
//        }
//
//        DayTimeSecondConverter converter = new DayTimeSecondConverter(seconds);
//
//        int day = converter.getDay();
//        long hours = converter.getHour();
//        long minute = converter.getMin();
//        long second = converter.getSeconds();
//
//        StringBuilder remainingTime = new StringBuilder();
//
//        if (day != 0) remainingTime.append(day).append("day ");
//
//        if (hours != 0) remainingTime.append(hours).append("h");
//
//        if (minute != 0) remainingTime.append(minute).append(" min ");
//
//        remainingTime.append(second).append("s");
//
//
//        if (mCurrentBottomFragment instanceof StartFragment) {

//            ViewSwitcher viewSwitcher = (ViewSwitcher) findViewById(R.id.vs_start);
//            TextView distanceTextView = (TextView) findViewById(R.id.tv_distance);
//            TextView arrivalTime = (TextView) findViewById(R.id.tv_arrival_time);
        Button start = (Button) findViewById(R.id.btn_start);

//            ConstraintLayout constraintLayout = (ConstraintLayout) findViewById(R.id.ct_layout_progressBar);
//
//            if (viewSwitcher.getCurrentView() == constraintLayout) {
//                viewSwitcher.showNext();
//                start.setVisibility(View.VISIBLE);
//                distanceTextView.setText(totalDistance);
//                arrivalTime.setText(remainingTime.toString());
//            }
//        }
    }

    @Override
    public void showError(String message) {

        Bundle bundle = new Bundle();

        bundle.putString(MyFragmentList.ERROR_MESSAGE, message);

        switchHeaderFragment(MyFragmentList.ERROR_FRAGMENT, bundle);

    }

    public String getDestinationName() {
        return destinationName;
    }

    private void centerRoute() {
        LatLng origin = new LatLng(presenter.getCurrentLocation().getLatitude(),
                presenter.getCurrentLocation().getLongitude());

        LatLng destination = new LatLng(presenter.getCurrentLocation().getLatitude(),
                presenter.getCurrentLocation().getLongitude());

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        builder.include(origin);

        for (Marker m : markerList) {
            builder.include(m.getPosition());
        }

        builder.include(destination);

        LatLngBounds bounds = builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mGeofencingClient.removeGeofences(getGeofencePendingIntent())
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences removed
                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to remove geofences
                        // ...
                    }
                });
    }


}


//    @Override
//    public void switchRecyclerViewContent(String toFragment, Bundle bundle) {
//
//
//
//        switch (toFragment) {
//            case MyFragmentList.START_DIRECTION:
//                Log.d(TAG, "start fragment: called");
//
//                RecyclerView mRecyclerView = findViewById(R.id.rv_start_direction);
//
//                LinearLayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
//                mRecyclerView.setLayoutManager(mLayoutManager);
//
//                // specify an adapter (see also next example)
//                String[] myDataset = {"1","2","3"};
//                RouteAdapter mAdapter = new RouteAdapter(myDataset);
//                mRecyclerView.setAdapter(mAdapter);
//
////                StartFragment startFragment = new StartFragment();
////                mCurrentBottomFragment = startFragment;
////
////                startFragment.setArguments(bundle);
////                getSupportFragmentManager().beginTransaction()
////                        .setCustomAnimations(R.anim.show_direction_slide_in, R.anim.show_direction_slide_out)
////                        .add(R.id.container_layout, startFragment)
////                        .commitAllowingStateLoss();
//                break;
//
//            case MyFragmentList.BOTTOM_DISTANCE_TIME_FRAG:
//                BottomDistanceTimeFragment fragment = new BottomDistanceTimeFragment();
//                mCurrentBottomFragment = fragment;
//
//                Bundle bundle1 = new Bundle();
//                bundle1.putString("destination_name", mDestinationStop.getStopName());
//                fragment.setArguments(bundle1);
//
////                getSupportFragmentManager().beginTransaction()
////                        .setCustomAnimations(R.anim.start_slide_in, R.anim.start_slide_out)
////                        .replace(R.id.container_layout, fragment)
////                        .commitAllowingStateLoss();
//
//                break;
//            default:
//                break;
//        }
//        Log.d(TAG, "switchRecyclerViewContent: ends");
//    }