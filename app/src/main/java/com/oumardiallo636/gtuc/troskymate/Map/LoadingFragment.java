package com.oumardiallo636.gtuc.troskymate.Map;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oumardiallo636.gtuc.troskymate.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoadingFragment extends Fragment {

    private View view;
    ViewGroup.LayoutParams params;

    private static final String TAG = "LoadingFragment";

    public LoadingFragment() {
        // Required empty public constructor
    }

    public void setView(int value) {

        params.height = value;
        view.setLayoutParams(params);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_loading, container, false);
        params = view.getLayoutParams();

        Log.d(TAG, "onCreateView: "+view.getHeight());

        return view;
    }

}
