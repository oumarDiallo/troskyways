package com.oumardiallo636.gtuc.troskymate.Map;


import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oumardiallo636.gtuc.troskymate.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartFragment extends Fragment
        implements ViewHelper.StartFragmentCallback{


    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private BottomSheetBehavior bottomSheetBehavior;

    @BindView(R.id.rv_start_direction)
    RecyclerView mRecyclerView;


    public StartFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.start_direction_layout,
                container,
                false);

        ButterKnife.bind(this, view);

        view.post(new Runnable() {
            @Override
            public void run() {
                int height = view.getMeasuredHeight() - 32; // for instance
                MainActivity.getInstance().changeMapBottomPadding(height);
            }
        });

        MainActivity.getInstance().setVisibilityNavigationStartFrag(true);

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new CustomRvLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL,
                false);


        mRecyclerView.setLayoutManager(mLayoutManager);


        // specify an adapter (see also next example)

        String[] myDataset = {"1","2","3"};

//        mAdapter = new RouteAdapter(myDataset);
//        mRecyclerView.setAdapter(mAdapter);

        return view;

    }


}
