package com.oumardiallo636.gtuc.troskymate.Map;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.oumardiallo636.gtuc.troskymate.R;
import com.oumardiallo636.gtuc.troskymate.Utility.MyFragmentList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShowDirection extends BottomSheetDialogFragment {

    private List<String> stop_names = new ArrayList<>();
    private List<String> stop_locations = new ArrayList<>();
    private String origin_stop;

    private View originView;
    private TextView currentStopName;
    private ImageView currentStopImage;

    private static final String TAG = "ShowDirection";

    @BindView(R.id.stop1_name)
    TextView stop1Name;

    @BindView(R.id.stop2_name)
    TextView stop2Name;

    @BindView(R.id.stop3_name)
    TextView stop3Name;

    @BindView(R.id.stop4_name)
    TextView stop4Name;

    @BindView(R.id.stop_1_img)
    ImageView imgStop1;

    @BindView(R.id.stop_2_img)
    ImageView imgStop2;

    @BindView(R.id.stop_3_img)
    ImageView imgStop3;

    @BindView(R.id.stop_4_img)
    ImageView imgStop4;

    @BindView(R.id.request_route)
    ImageView requestRoute;



    @OnClick(R.id.stop_one)
    public void stop1Click(View view){

        int index = 0;

        double lat = Double.parseDouble(stop_locations.get(index).split(",")[0]);
        double lng = Double.parseDouble(stop_locations.get(index).split(",")[1]);
        LatLng location = new LatLng(lat,lng);

        imgStop1.setImageResource(R.drawable.logo_image);
        setOrigin(view,stop1Name,imgStop1,0);
        requestRoute.setVisibility(View.VISIBLE);

        MainActivity.getInstance().showMarker(location,"origin",stop_names.get(index));

    }

    @OnClick(R.id.stop_two)
    public void stop2Click(View view){
        int index = 1;

        double lat = Double.parseDouble(stop_locations.get(index).split(",")[0]);
        double lng = Double.parseDouble(stop_locations.get(index).split(",")[1]);
        LatLng location = new LatLng(lat,lng);

        setOrigin(view,stop2Name,imgStop2,1);
        requestRoute.setVisibility(View.VISIBLE);
        MainActivity.getInstance().showMarker(location,"origin",stop_names.get(index));
    }


    @OnClick(R.id.stop_three)
    public void stop3Click(View view){
        int index = 2;

        double lat = Double.parseDouble(stop_locations.get(index).split(",")[0]);
        double lng = Double.parseDouble(stop_locations.get(index).split(",")[1]);
        LatLng location = new LatLng(lat,lng);

        setOrigin(view,stop3Name,imgStop3,2);
        requestRoute.setVisibility(View.VISIBLE);
        MainActivity.getInstance().showMarker(location,"origin",stop_names.get(index));
    }

    @OnClick(R.id.stop_four)
    public void stop4Click(View view){
        int index = 3;

        double lat = Double.parseDouble(stop_locations.get(index).split(",")[0]);
        double lng = Double.parseDouble(stop_locations.get(index).split(",")[1]);
        LatLng location = new LatLng(lat,lng);

       setOrigin(view,stop4Name,imgStop4,3);
       requestRoute.setVisibility(View.VISIBLE);
        MainActivity.getInstance().showMarker(location,"origin",stop_names.get(index));
    }



    @OnClick(R.id.request_route)
    public void request_route(){
        MainActivity.getInstance().searchDirection(origin_stop);
        MainActivity.getInstance().switchHeaderFragment(MyFragmentList.LOADING_FRAGMENT,null);
//        MainActivity.getInstance().hideBottomFragment();
        this.dismiss();

    }


    public ShowDirection() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.origin_stop_layout, container, false);

        ButterKnife.bind(this, view);


        if( getArguments() != null){
            stop_names = getArguments().getStringArrayList("stop_names");
            stop_locations = getArguments().getStringArrayList("stop_locations");

            stop1Name.setText(stop_names.get(0));
            stop2Name.setText(stop_names.get(1));
            stop3Name.setText(stop_names.get(2));
            stop4Name.setText(stop_names.get(3));
        }


        view.post(new Runnable() {
            @Override
            public void run() {
                int height = view.getMeasuredHeight() + 8; // for instance
                MainActivity.getInstance().changeMapBottomPadding(height);
            }
        });

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: starts");
        super.onSaveInstanceState(outState);

        Log.d(TAG, "onSaveInstanceState: ends");
    }

    private void setOrigin(View view,TextView stopName, ImageView stopImage, int value){

        if (originView != null){
            originView.setBackgroundResource(0);
            ViewCompat.setElevation(originView,0);
        }

        if (currentStopImage != null){
            currentStopImage.setImageResource(R.drawable.dead_logo);
        }

        if (currentStopName != null){
            currentStopName.setTextColor(getResources().getColor(R.color.divider));
        }

        ViewCompat.setElevation(view,6);
        origin_stop = stop_locations.get(value);
        view.setBackgroundResource(R.drawable.rounded_border_bottom);


        stopName.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        stopImage.setImageResource(R.drawable.logo_image);

        originView = view;
        currentStopImage = stopImage;
        currentStopName = stopName;

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        MainActivity.getInstance().clearMap();
    }

}
